# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store,
                                       key: '_yodleur_session',
                                       expire_after: 20.minutes,
                                       secret: :secret_key_base
