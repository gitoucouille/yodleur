require 'rufus-scheduler'

# Toutes les heures avec la première fois la première seconde
Rufus::Scheduler.singleton.interval('1h', first_in: '1') do
  MotNuage.destroy_all
  construire_nuage
end

NOMBRE = 30
TAILLE_MAX = 40
TAILLE_MIN = 10
POIDS_TITRE = 2

def construire_nuage
  mots = {}
  # {'mot' => poids}
  # [\p{L}] = lettre pour n'importe quel language
  compter_mots(mots, Groupe.en_cours.collect{|g| g.categorie_principale.nom}, 10)
  compter_mots(mots, Groupe.en_cours.where{categorie_secondaire_id != nil}
                         .includes(:categorie_secondaire).collect{|g| g.categorie_secondaire.nom}, 4)
  compter_mots(mots, Sujet.en_cours.pluck(:titre), 2)
  compter_mots(mots, Sujet.en_cours.pluck(:description), 1)


  mots_frequents = File.readlines(Rails.root + 'lib/assets/mots_frequents.txt').map(&:strip)

  mots_choisis = mots.values.select{|m| !mots_frequents.include? m.mot}.sort_by{|m| -m.frequence}.take(NOMBRE)
  unless mots_choisis.empty?
    freq_max = mots_choisis.first.frequence
    freq_min = mots_choisis.last.frequence
    mots_choisis.each do |m|
      m.poids = (m.frequence - freq_min) * (TAILLE_MAX - TAILLE_MIN) / ([freq_max - freq_min, 1].max) + TAILLE_MIN
      m.save
    end
  end
end

private
def ajout_occurrence(mots, mot, nb)
  mot.downcase!
  if mots[mot].nil?
    mots[mot] = MotNuage.new(mot: mot, frequence: nb)
  else
    mots[mot].frequence += nb
  end
end

private
def chaines_en_mots(chaines)
  chaines.join(' ').split(/[^'\p{L}]+/)
end

private
def compter_mots(liste_mots, chaines, poids)
  chaines_en_mots(chaines).each {|mot| ajout_occurrence(liste_mots, mot, poids)}
end