require 'httparty'

url = 'http://localhost:3001/recuperation_cle_publique/'
response = HTTParty.get(url)

modulus = response.parsed_response['modulus']
exponent = response.parsed_response['exponent']

File.open('cle_publique_serveur', "w+") do |f|
  f.write(exponent)
  f.write(modulus)
end
