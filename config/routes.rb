
Rails.application.routes.draw do
  default_url_options :host => "localhost:3000"
  root 'pages#accueil'

  # menu
  get '/accueil' => 'pages#accueil'
  get '/proposer'  => 'groupes#new'
  get '/mes_projets'  => 'groupes#index_mygroups'
  get '/voter' => 'groupes#index'
  get '/resultats' => 'groupes#index_outdated'
  get '/liste_utilisateurs' => 'utilisateurs#index'
  get '/a_propos' => 'pages#a_propos'

  # ressources
  resources :utilisateurs
  resources :delegations
  resources :sessions
  resources :groupes
  resources :lieux
  resources :categories
  resources :sujets do
    resources :commentaires
  end



  # routes de sujet
  get '/ammendements/:id/new' => 'sujets#nouvel_ammendement', as: :nouvel_ammendement
  post '/ammendements' => 'sujets#creer_ammendement', as: :creer_ammendement

  patch '/sujets/:id/incremente_votes_pour' => 'sujets#incremente_votes_pour', as: :incremente_votes_pour
  patch '/sujets/:id/incremente_votes_contre' => 'sujets#incremente_votes_contre', as: :incremente_votes_contre
  patch '/sujets/:id/incremente_votes_blancs' => 'sujets#incremente_votes_blancs', as: :increase_votes_blancs

  get '/envoi_vote/:sujet/:vote' => 'sujets#envoi_vote'

  # session
  get 'sessions/new'
  get 'log_out' => 'sessions#destroy', :as => 'log_out'
  get 'log_in' => 'sessions#new', :as => 'log_in'
  get 'sign_up' => 'utilisateurs#new', :as => 'sign_up'

  # graphe
  get 'groupes/:id/graphe' => 'groupes#graphe', :as => 'graphe'

  # administration
  get 'admin' => 'admin#index'

  #routes d'acceptance d'inscription, pour l'utilisateur :id avec la clé de vérification :cle
  get '/accept/:id/:cle' => 'utilisateurs#verifier_mairie', as: :verifier_mairie

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
