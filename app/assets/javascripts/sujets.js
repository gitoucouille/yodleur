//var bigInt = require("big-integer");

function envoi_votes(sujet, modulus, exponent, vote){
    var cle_publique = new RSAKey();

    var mod_int = "da36a556c486cf6ade0298635636c97d";
    var exp_int = "10001";

    cle_publique.setPublic(modulus, exponent);

    var sel = Math.floor(Math.random() * 10);
    var a_chiffrer = "" + sel + vote;

    var paquet = cle_publique.encrypt(a_chiffrer);
    this.location = '/envoi_vote/' + sujet + '/' + paquet;
}
