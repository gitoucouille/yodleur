function graphe(selecteur){

  $.ajax({
    type: "GET",
    url: this.location + "/graphe.json",
    dataType: "json",
    success: function(donnees){
      dessiner(donnees);
    },
    error: function(erreur){
      console.warn("Impossible de contacter le serveur : " + erreur);
    }
  });

  var rayon = 15;

  var svg = d3.select(selecteur)
    .append("svg")
    .attr("width", "100%")
    .attr("class", "graphe");

  function width(){ return $(svg[0]).width(); }
  function height(){ return $(svg[0]).height(); }

  var force = d3.layout.force()
    .charge(-100)
    .linkDistance(rayon * 5) // approximation

  function resize(){  force.size([width(), height()]).resume(); }

  function dessiner(json){

    function votes(d){ return d.votes_pour + d.votes_blancs + d.votes_contre; }

    svg.attr("height", Math.sqrt(json.links.length * rayon * 500)); // approximation

    force
      .nodes(json.nodes)
      .links(json.links)
      .start();

    resize();

    var links = svg.selectAll(".link")
      .data(json.links)
      .enter()
      .append("line")
      .attr("class", "link");

    var nodes = svg.selectAll(".node")
      .data(json.nodes)
      .enter()
      .append("g")
      .attr("data-id", function(d){ return d.id; })
      .attr("class", "node_group")
      .call(force.drag);

    nodes.append("circle")
      .attr("class", "node")
      .attr("r", function(d){ return rayon + Math.sqrt(votes(d)); })
      .attr("fill", function(d){ return (d.parent_id == null) ? "#ff69b4" : "#333"; }) // Si le nœud est racine

    nodes.append("text")
      .attr("dy", ".35em")
      .attr("text-anchor", "middle")
      .attr("class", "node_text")
      .text(function(d){ return votes(d); });

    nodes.append("title")
      .text(function(d){ return d.titre; });

    force.on("tick", function() {
      links.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

      nodes.attr("transform", function(d) {
        var nx = function(){
          nx = d.x;
          if(nx + rayon > width()) nx = width() - rayon;
          if(nx < 0) nx = 0;
          return nx;
        }();
        var ny = function(){
          ny = d.y;
          if(ny + rayon > height()) ny = height() - rayon;
          if(ny < 0) ny = 0;
          return ny;
        }();
				return "translate(" + nx + "," + ny + ")";
			});
    });

    // Magouille pour que les nœuds soient déplaçables ET cliquables
    var dernierClic;
    $(".node_group")
      .mouseup(function(event){
        // Clic gauche et assez longtemps qu'on a cliqué (pour représenter un déplacement)
        if(event.which == 1 && (Date.now() - dernierClic) < new Date(100))
          document.location = "/sujets/" + $(this).data("id");
      })
      .mousedown(function(){
        dernierClic = Date.now();
      })
      .mouseup()

  }

  // Permet de recentrer le graphe lors du redimensionnement de la fenêtre
  $(window).resize(function() {
    resize();
  });

}
