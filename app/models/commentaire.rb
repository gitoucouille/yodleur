class Commentaire < ActiveRecord::Base
  belongs_to :sujet
  belongs_to :auteur,
             class_name: 'Utilisateur'

  belongs_to :parent,
      class_name: 'Commentaire'

  has_many :reponses,
      class_name: 'Commentaire',
      foreign_key: 'parent_id'
end
