class Groupe < ActiveRecord::Base
  has_many :sujets, :dependent => :delete_all

  belongs_to :categorie_principale,
             class_name: 'Categorie'
  belongs_to :categorie_secondaire,
             class_name: 'Categorie'
  belongs_to :lieu

  # Recherche de tous les groupes dont les sujets ou les catégories sont en rapport
  def self.search(search)
    categories_ids = Categorie.search(search).ids
    where{(id.in(Sujet.search(search).select{groupe_id})) |
        (categorie_principale_id.in(categories_ids)) |
        (categorie_secondaire_id.in(categories_ids))}
  end

  def votes_total
    self.sujets.inject(0) do |total, sujet|
      total + sujet.nombre_votants
    end
  end

  def createur
    self.sujets.first.createur
  end

  def en_cours?
    DateTime.now.between? self.date_debut, self.date_fin
  end

  def self.en_cours
    where{(date_debut <= DateTime.now) & (date_fin >= DateTime.now)}
  end

  def pourcentage_restant
    termine = date_fin - date_debut
    en_cours = (Time.current - date_debut)
    pourcentage = 100 - ((en_cours * 100) / termine).to_i
    if pourcentage > 100
      pourcentage = 100
    elsif pourcentage < 0
      pourcentage = 0
    end
    pourcentage
  end

  def temps_restant
    if termine?
      {jours: 0, heures: 0, minutes: 0}
    else
      t = (date_fin - Time.current).to_i
      mm, ss = t.divmod(60)
      hh, mm = mm.divmod(60)
      dd, hh = hh.divmod(24)
      {jours: dd, heures: hh, minutes: mm}
    end
  end

  def temps_restant_str
    "%d jours, %d heures, %d minutes" % [temps_restant[:jours], temps_restant[:heures], temps_restant[:minutes]]
  end

  def termine?
    date_fin < Time.current
  end

  def description_courte
    self.sujets.first.description_courte
  end

  def victoire?
    self.sujets.first.victoire?
  end

end
