class UtilisateurMailer < ApplicationMailer
  default from: 'picdeballmer@gmail.com'

  def welcome_email(user)
    @utilisateur = user

    mail(to: @utilisateur.mail,
         subject: 'Bienvenue sur Yodleur !')
  end

  def verif_mairie(user)
    @utilisateur = user
    # TODO : envoyer le mail à la bonne mairie
    mail(to: 'otfaure@hotmail.fr',
         subject: 'Nouvelle demande d\'inscription sur Yodleur')
  end

  def verif_mairie_reussie(user)
    @utilisateur = user
    mail(to: @utilisateur.mail,
         subject: 'Votre inscription sur Yodleur a été validée')
  end

end
