class ApplicationMailer < ActionMailer::Base
  default from: 'picdeballmer@gmail.com'
  layout 'mailer'
end
