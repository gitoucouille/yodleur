class PagesController < ApplicationController

  def accueil
  end

  def proposer
  end

  def voter
  end

  def resultats
  end
  
  def profil
  end

  def liste_utilisateurs    
  end

  def a_propos
  end

end
