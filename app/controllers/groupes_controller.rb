# coding: utf-8
class GroupesController < ApplicationController

  before_action :require_login, only: [:new, :edit, :create]
  before_action :require_admin, only: :destroy

  respond_to :html, :json

  # GET /groupes
  def index
    if params[:search]
      @groupes = Groupe.search(params[:search]).order(created_at: :desc)
    else
      @groupes = Groupe.order(created_at: :desc)
    end
    @groupes = @groupes.select { |g| !g.termine? }
    @groupes_pelos, @groupes_institutionnels = @groupes.partition { |g| g.createur.pelo? }
  end

  def index_outdated
    if params[:search]
      @groupes = Groupe.search(params[:search]).order(created_at: :desc)
    else
      @groupes = Groupe.order(created_at: :desc)
    end
    @groupes = @groupes.select { |g| g.termine? }
    @groupes_pelos, @groupes_institutionnels = @groupes.partition { |g| g.createur.pelo? }
  end

  def index_mygroups
    if params[:search]
      @groupes = Groupe.search(params[:search]).order(created_at: :desc)
    else
      @groupes = Groupe.order(created_at: :desc)
    end
    @groupes = @groupes.select { |g| g.createur == utilisateur_courant }
    @termines, @en_cours = @groupes.partition { |g| g.termine? }
  end

  # A SUPPRIMER SI TOUT MARCHE
  #alias_method :index_outdated, :index
  #alias_method :index_mygroups, :index

  # GET /groupes/1
  def show
    @groupe = Groupe.find_by_id(params[:id])
  end

  # GET /groupes/new
  def new
    @groupe = Groupe.new
    @sujet = Sujet.new
    # redirect_to controller: sujets_url, action: new #, :titre => titre
    # redirect_to use_route: 'sujets/new'
  end

  # GET /groupes/1/edit
  def edit
  end

  # POST /groupes
  def create
    @groupe = Groupe.new(groupe_params)
    if @groupe.save
      @sujet = Sujet.new(sujet_params)
      @sujet.createur = utilisateur_courant
      @sujet.votes_pour = 0
      @sujet.votes_blancs = 0
      @sujet.votes_contre = 0
      @sujet.groupe = @groupe
      @sujet.titre = @groupe.titre
      if @sujet.save
        redirect_to @groupe, notice: 'Groupe was successfully created.'
      else
        @groupe.destroy
        render :new
      end
    else
      render :new
    end
  end

  def destroy
    groupe = Groupe.find_by_id(params[:id])
    unless groupe.nil?
      groupe.destroy
    end
    redirect_to :groupes
  end

  def graphe
    @groupe = Groupe.find(params[:id])
    respond_to do |format|
      format.json {

        nodes = @groupe.sujets
        links = nodes.select {|node| !node.parent.nil?}.collect do |node|
          {
            source: nodes.index(node),
            target: nodes.index(node.parent)
          }
        end

        render json: {
            nodes: nodes,
            links: links
          }
      }
    end
  end

  private
    def groupe_params
      params.require(:groupe).permit(:titre,
        :date_debut,
        :date_fin,
        :categorie_principale_id,
        :categorie_secondaire_id)
    end

    def sujet_params
      params.require(:sujet).permit(:description)
    end

end
