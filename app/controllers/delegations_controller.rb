class DelegationsController < ApplicationController
  before_action :require_login

  def new
    @delegation = Delegation.new
    unless params[:receveur_id].nil?
      @delegation.receveur_id = params[:receveur_id].to_i
    end
  end

  # Nouvelle délégation
  def create
    @delegation = Delegation.new delegation_params.merge(donneur_id: utilisateur_courant.id)

    if @delegation.save
      redirect_to utilisateur_path(utilisateur_courant)
    else
      render 'new'
    end
  end

  def destroy
    @delegation = Delegation.find_by_id(params[:id])

    if @delegation != nil
      @delegation.destroy
    end

    redirect_to utilisateur_path(utilisateur_courant)
  end

  private
    def delegation_params
      params.require(:delegation)
          .permit(:donneur_id, :receveur_id, :categorie_id)
    end

end
