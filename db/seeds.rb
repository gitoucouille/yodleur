# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

categories = Categorie.create([{nom: 'Sport'}, {nom: 'Culture'}, {nom: 'Numérique'}, {nom: 'Environnement'}, {nom: 'Social'}, {nom: 'Éducation'}])

lieus = Lieu.create([{nom: 'Villeurbanne'}, {nom: 'Lyon 1'}, {nom: 'Lyon 2'}, {nom: 'Lyon 3'}, {nom: 'Lyon 4'}])

# utilisateurs à créer à la main, parce que c'est la merde avec les mdp

groupes = Groupe.create([{titre: 'Améliorer le WiFi dans les salles de TP'}, {titre: 'Installer une tireuse à bière en Gaston Berger'}, {titre: 'Planter des arbres sur les humas'}, {titre: 'Organiser un concert de Barbiche'}])

