class CreateMotNuages < ActiveRecord::Migration
  def change
    create_table :mot_nuages do |t|
      t.string :mot
      t.integer :frequence
      t.integer :poids

      t.timestamps null: false
    end
  end
end
