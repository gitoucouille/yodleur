class CreateDelegations < ActiveRecord::Migration
  def change
    create_table :delegations do |t|
      t.references :donneur, index: true, foreign_key: true
      t.references :receveur, index: true, foreign_key: true
      t.references :categorie, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :delegations, [:donneur_id, :categorie_id], unique:true
  end
end
