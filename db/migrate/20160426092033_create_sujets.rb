class CreateSujets < ActiveRecord::Migration
  def change
    create_table :sujets do |t|
      t.string :titre
      t.text :description
      t.binary :image
      t.string :etat
      t.datetime :date_creation
      t.datetime :date_debut
      t.datetime :date_fin
      t.integer :votes_pour
      t.integer :votes_contre
      t.integer :votes_blancs
      t.references :createur, index: true, foreign_key: true
      t.references :categorie_principale, index: true, foreign_key: true
      t.references :categorie_secondaire, index: true, foreign_key: true
      t.references :lieu, index: true, foreign_key: true
      t.references :parent, index: true, foreign_key: true
      t.references :groupe, index: true, foreign_key: true

      t.timestamps null: false
    end

    create_table :votes, id: false do |t|
      t.belongs_to :sujet, index: true, foreign_key: true
      t.belongs_to :utilisateur, index: true, foreign_key: true
    end

    add_index :votes, [:sujet_id, :utilisateur_id], unique:true #unicité de la paire

  end
end
