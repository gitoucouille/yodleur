class CreateAttentes < ActiveRecord::Migration
  def change
    create_table :attentes do |t|
      t.integer :vote
      t.integer :sujet

      t.timestamps null: false
    end
  end
end
