class CreateUtilisateurs < ActiveRecord::Migration
  def change
    create_table :utilisateurs do |t|
      t.string :nom
      t.string :prenom
      t.string :mail
      t.integer :droits
      t.references :categorie, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
