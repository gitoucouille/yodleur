class AddLieuDeNaissanceToUtilisateur < ActiveRecord::Migration
  def change
    add_column :utilisateurs, :lieu_de_naissance, :string
  end
end
