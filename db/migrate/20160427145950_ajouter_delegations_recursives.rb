class AjouterDelegationsRecursives < ActiveRecord::Migration
  def change
    change_table :delegations do |t|
      t.integer :rec_profondeur
      t.references :rec_racine, index: true, foreign_key: true
    end
  end
end
