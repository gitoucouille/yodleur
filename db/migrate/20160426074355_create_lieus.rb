class CreateLieus < ActiveRecord::Migration
  def change
    create_table :lieus do |t|
      t.string :nom

      t.timestamps null: false
    end
  end
end
