class AddLieuToUtilisateurs < ActiveRecord::Migration
  def change
    add_reference :utilisateurs, :lieu, index: true, foreign_key: true
  end
end
