class CreateCommentaires < ActiveRecord::Migration
  def change
    create_table :commentaires do |t|
      t.text :texte
      t.datetime :date
      t.integer :plus
      t.integer :moins
      t.references :sujet, index: true, foreign_key: true
      t.references :auteur, index: true, foreign_key: true
      t.references :parent, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
