class AddCiviliteToUtilisateurs < ActiveRecord::Migration
  def change
    add_column :utilisateurs, :civilite, :int
  end
end
