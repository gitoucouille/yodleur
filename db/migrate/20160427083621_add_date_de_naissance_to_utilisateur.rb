class AddDateDeNaissanceToUtilisateur < ActiveRecord::Migration
  def change
    add_column :utilisateurs, :date_de_naissance, :date
  end
end
