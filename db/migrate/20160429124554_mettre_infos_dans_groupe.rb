class MettreInfosDansGroupe < ActiveRecord::Migration
  def change
    add_column :groupes, :date_debut, :datetime
    add_column :groupes, :date_fin, :datetime
    add_column :groupes, :categorie_principale_id, :integer
    add_column :groupes, :categorie_secondaire_id, :integer
    add_column :groupes, :lieu_id, :integer

    Sujet.find_each do |sujet|
      groupe = Groupe.find_by_id(sujet.groupe_id)
                   unless groupe.nil?
                     groupe.update_attributes(
                        date_debut: sujet.date_debut,
                        date_fin: sujet.date_fin,
                        categorie_principale_id: sujet.categorie_principale_id,
                        categorie_secondaire_id: sujet.categorie_secondaire_id,
                        lieu_id: sujet.lieu_id)
                   end
    end

    remove_columns :sujets, :date_debut, :date_fin, :categorie_secondaire_id, :categorie_principale_id, :lieu_id
  end
end
